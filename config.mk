# Note: In the default version of this file, commented out values
#       indicate what the GNU standards dictate, when our values
#       differ.  We're not a GNU package.

ifeq ($(origin topsrcdir),undefined)
topsrcdir := $(patsubst %/,%,$(dir $(lastword $(MAKEFILE_LIST))))
topoutdir := $(topsrcdir)

# In case .srcversion-libretools.mk hasn't been generated yet.
LIBRETOOLS_VERSION ?= 0

PACKAGE = libretools
VERSION = $(LIBRETOOLS_VERSION)

DESTDIR =

#prefix      = /usr/local
prefix      = /usr
exec_prefix = $(prefix)
bindir      = $(exec_prefix)/bin
#libexecdir  = $(exec_prefix)/libexec
libexecdir  = $(exec_prefix)/lib

datarootdir = $(prefix)/share
datadir     = $(datarootdir)
#sysconfdir  = $(prefix)/etc
sysconfdir  = /etc

docdir = $(datarootdir)/doc
mandir = $(datarootdir)/man

devtoolsdir = $(call pkgpath,$(topsrcdir)/../devtools)
RONNFLAGS = --manual='libretools Manual' --organization='Hyperbola Project'

TESTENVFLAGS ?=

.LIBPATTERNS ?=

endif
