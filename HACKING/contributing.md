Contributing
============

Patches should be sent to <devel@hyperbola.info>; please put
"[PATCH]" and "libretools" in the subject line.  If you have commit
access, but want me to look over it first, feel free to create a new
branch in git.  Try to avoid pushing to the "master" branch unless
it's a trivial change; it makes it easier to review things.

Be sure to make sure to follow the licensing requirements in
`HACKING/licensing.md`

Please discuss possible changes on IRC (#hyperbola).

Please write unit tests for new functionality.  Or old functionality.
Please write unit tests!  See `HACKING/testing.md` for details on
testing.
