The build system is built around an Automake-replacing Makefile
snippet system for GNU Make, called Autothing.
It is documented in `build-aux/Makfile.README.txt`.  It provides all
of the standard targets and such; you tell it what to do by setting a
series of `at.whatever` or `module.whatever` variables.

There are a couple of variables that get automatically set from git.
This happens by `include`ing a hidden makefile that sets them; if
`$(topsrcdir)/.git` exists, it knows to use git to generate it;
otherwise it expects the file to exist:

| variable             | file                                     |
|----------------------+------------------------------------------|
| `files.src.src`      | `$(topsrcdir)/.srcfiles.mk`              |
| `LIBRETOOLS_VERSION` | `$(topsrcdir)/.srcversion-libretools.mk` |
| `LIBRETOOLS_COMMIT`  | `$(topsrcdir)/.srcversion-libretools.mk` |
| `DEVTOOLS_VERSION`   | `$(topsrcdir)/.srcversion-devtools.mk`   |
| `DEVTOOLS_COMMIT`    | `$(topsrcdir)/.srcversion-devtools.mk`   |

Now, the `Makefile`s in each directory are pretty bare.  Most of
everything can be guesses by inspecting `$(files.src.src)` (Want to
add a shell program?  Just commit the executable; the Makefile will
see it.)  So, most of the real Makefile stuff is in the files
`build-aux/Makefile.*/??-libretools.mk`.
