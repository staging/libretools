Testing
=======

Please write unit tests for new things.  Tests can be run with `make
check`, which just runs `./testenv roundup` in the `test/` directory.
Relatedly, you need the `roundup` (the `sh-roundup` package)
tool to run the tests.  `./testenv` can be given
`--no-network` and/or `--no-doas` to dissable tests that require those
things.  Make can be made to pass those things in by setting
`TESTENVFLAGS`.  If you don't dissable either, It's *strongly*
recommended setting TMPDIR to somewhere on a btrfs partition before
running the tests; otherwise the chroot tests will take forever.

It's recommended to use the `haveged` service, but also some of
the tests make GPG keys, this "should" take on the order of 1 second,
but can take several minutes if you don't have `haveged` running.
