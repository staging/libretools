Style guidelines
================

Use a litteral tab for indent.  When indenting line-wrapped text, such
as that for `prose`, do it like this:  (» indicates tab, · indicates
space)

	func() {
	»   prose "This is the first line. This paragraph is going to be
	»   ·······wrapped."
	}

The `; then` and `; do` should go on the same line as
`if`/`elif`/`for`/`while`.  Also, there is no space before the `;`.

You should use the `init; while cond; do exec; inc; done` syntax over
the `for ((init; cond; inc)); do exec; done`syntax.
For example (heh, `for` example):

	local i
	for (( i = 1 ; i <= 10 ; i++ )); do
		...
	done

should be

	i=1
	while [ $i -le 10 ]; do
		...
		i=$((i + 1))
	done
	unset i

Of course, if the upper bound is a variable, the C-like syntax is
the better option, as otherwise you would have to use `seq` (calling
an external), or `eval` (gross, easy to mess up royally).

Indent comments like you would code; don't leave them at the beginning
of the line.  Example:

	for item in "${list[@]}"; do
		case ${item} in
		'foo')
	# BAD
			foobar
			;;
		esac
		case ${item} in
		'bar')
			# GOOD
			barbaz
			;;
		esac
	done

Some people argue in favor of the useless use of cat, because data
should flow from left to right.  However, the input redirection
doesn't have to go on the right side of a command:

	cat file | program  # useless use of cat
	program < file      # data flows right to left
	< file program      # just right
