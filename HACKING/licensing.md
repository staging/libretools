We don't require copyright assignment or any fancy paperwork!  Just
make sure you specify the license and include a copyright statement
with your name and the current year.

New code should be licensed GPLv3+ or any compatible license.

Copyright statements should look like

	# Copyright (C) YEARS NAME <EMAIL>

for most code, for 3rd-party code that has been imported, indent it a
bit:

	#   Copyright (C) YEARS NAME <EMAIL>

Always put a line with `# License:` specifying the license of that
file.
