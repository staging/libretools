include $(dir $(lastword $(MAKEFILE_LIST)))/config.mk
include $(topsrcdir)/build-aux/Makefile.head.mk

# these are the resulting packages
pkgs=libretools librelib
# and which directories they contain
pkg.libretools=\
	src \
	src/package-tools \
	src/chroot-tools \
	src/devtools \
	src/librefetch \
pkg.librelib=src/lib

define _pkg_rule
%(verb)-%(pkg): $(addsuffix /%(verb),$(pkg.%(pkg)))
.PHONY: %(verb)-%(pkg)
endef
$(eval $(foreach verb,$(nested.targets),$(foreach pkg,$(pkgs),\
  $(subst %(verb),$(verb),$(subst %(pkg),$(pkg),$(value _pkg_rule)))$(at.nl))))

$(outdir)/check:
	cd $(@D)/test && ./testenv $(TESTENVFLAGS) roundup

_po_rule = \
po/%(pkg).pot: $(addsuffix /everything.pot,$(pkg.%(pkg))); \
	cat $^
$(eval $(foreach pkg,$(pkgs),\
  $(subst %(pkg),$(pkg),$(value _po_rule))$(at.nl)))

libretools.out.mans =
libretools.out.bins =
libretools.out.libexecs =
libretools.out.libs =
libretools.out.docs =
libretools.out.confs =
libretools.out  = $(foreach pkg,$(pkgs),po/$(pkg).pot)

files.src.gen += .srcversion-libretools.mk .srcversion-devtools.mk
nested.subdirs = $(foreach pkg,$(pkgs),$(pkg.$(pkg)))

include $(topsrcdir)/build-aux/Makefile.tail.mk
