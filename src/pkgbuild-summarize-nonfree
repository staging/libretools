#!/bin/ksh

# Copyright (C) 2013, 2017 Luke Shumaker <lukeshu@sbcglobal.net>
#
# Modifications to support Hypertools:
# Copyright (C) 2022-2024 Hyperbola Project
#
# License: GNU GPLv3+
#
# This file is part of Parabola.
#
# Parabola is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parabola is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Parabola.  If not, see <http://www.gnu.org/licenses/>.

. "$(librelib messages)"

# Make sure these match pkgbuild-check-nonfree
readonly _E_OK=0
readonly _E_ERROR=1
readonly _E_LIC_UNKNOWN=2
readonly _E_LIC_NOGPL=4
readonly _E_LIC_WRONG=8

usage() {
	print "Usage: %s [OPTIONS] STATUS" "${0##*/}"
	print "Summarizes a status code from pkgbuild-check-nonfree"
	echo
	prose 'It thresholds the issues it finds, only failing for error-level
	       issues, and ignoring warnings.  Unless `-q` is specified, it also
	       prints a summary of the issues it found.'
	echo
	print 'Options:'
	flag '-q' 'Be quiet'
	flag '-h' 'Show this message'
}

main() {
	local quiet=false
	while getopts 'qh' arg; do
		case "$arg" in
			q) quiet=true;;
			h) usage; return 0;;
			*) usage >&2; return 1;;
		esac
	done
	shift $((OPTIND - 1))
	if [ $# -ne 1 ]; then
		usage >&2
		return 1
	fi
	case $(printf $1 | /bin/grep '^[0-9]\+$') in
	$1) :;;
	*) error 'STATUS must be an integer';;
	esac

	if $quiet; then
		error() { :; }
		warning() { :; }
	fi

	parse "$1"
}

parse() {
	[ $# -eq 1 ] || panic 'malformed call to parse'
	typeset -i s=$1;

	typeset -i ret=0
	typeset -i i
	for i in 1 2 4 8 16 32; do
		if [ $((s&i)) -gt 0 ]; then
			case $i in
				$_E_ERROR)
					# could be anything, assume the worst
					error "There was an error processing the PKGBUILD"
					ret=1;;
				$_E_LIC_UNKNOWN)
					warning "This PKGBUILD has an unknown license";;
				$_E_LIC_NOGPL)
					warning "This PKGBUILD has a GPL-incompatible license";;
				$_E_LIC_WRONG)
					error "This PKGBUILD has a uncommon, wrong typing or nonfree license"
					ret=1;;
			esac
		fi
	done
	return $ret
}

main "$@"
