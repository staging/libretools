#!/bin/ksh
# LibreStage
# Prepares packages for upload

# Copyright (C) 2010-2012 Nicolás Reynolds <fauno@parabola.nu>
# Copyright (C) 2011 Joshua Ismael Haase Hernández (xihh) <hahj87@gmail.com>
# Copyright (C) 2013-2014 Luke Shumaker <lukeshu@sbcglobal.net>
#
# Modifications to support Hypertools:
# Copyright (C) 2022-2024 Hyperbola Project
#
# License: GNU GPLv3+
#
# This file is part of Parabola.
#
# Parabola is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parabola is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Parabola. If not, see <http://www.gnu.org/licenses/>.

. libremessages
. "$(librelib conf.sh)"

usage() {
	print "Usage: %s [REPO]" "${0##*/}"
	print "Stages the package(s) build by ./PKGBUILD for upload."
	echo
	prose "The package(s) are staged for the named repository, or the name
	       of the parent directory if a repository is not named."
}

main() {
	if [ -w / ]; then
		error "This program should be run as a regular user"
		return 1
	fi

	# Parse options, set up
	while getopts 'h' arg; do
		case $arg in
			h) usage; return 0;;
			*) usage >&2; return 1;;
		esac
	done
	local repo=
	case $# in
		0) repo="$(basename "$(dirname "$PWD")")";;
		1) repo=$1;;
		*) usage >&2; return 1;;
	esac

	if ! [ -e ./PKGBUILD ]; then
		error "PKGBUILD not found"
		return 1
	fi

	# Load configuration
	load_files libretools
	check_vars libretools WORKDIR ARCHES || return 1
	load_files makepkg # for PKGDEST and SRCDEST, which are optional
	load_files librefetch # for MIRRORS, which is optional

	# Load the PKGBUILD
	load_PKGBUILD

	# Now for the main routine.
	local staged=false
	slock 8 "${WORKDIR}/staging.lock" \
		'Waiting for a shared lock on the staging directory'

	# Look for makepkg output
	local CARCH _pkgname pkgfile
	for CARCH in "${ARCHES[@]}" any; do
		for _pkgname in "${pkgname[@]}" "${pkgname[@]/%/-debug}"; do
			if ! pkgfile=$(find_cached_package "$_pkgname" "$(get_full_version "$_pkgname")" "$CARCH"); then
				continue
			fi

			msg 'Found package: %s' "${pkgfile##*/}"

			# This little check is from devtools:commitpkg
			if grep -q "packager = Unknown Packager" <(bsdtar -xOqf "$pkgfile" .PKGINFO); then
				die "PACKAGER wes not set when building package"
			fi

			mkdir -p "${WORKDIR}/staging/${repo}"
			if cp "$pkgfile" "${WORKDIR}/staging/${repo}/${pkgfile##*/}"; then
				msg2 "%s staged on [%s]" "$_pkgname" "$repo"
				staged=true
			else
				error "Can't put %s on [%s]" "$_pkgname" "$repo"
				return 1
			fi
		done
	done

	# Look for librefetch output
	local netfile mirror path
	local srcurl srcname srcpath
	for netfile in "${source[@]}"; do
		for mirror in "${MIRRORS[@]}"; do
			srcurl=${netfile#*::}
			case "$srcurl" in
			"$mirror"*)
				case $netfile in
				*::*)
					srcname=${netfile%%::*}
					;;
				*)
					srcname=${netfile##*/}
					;;
				esac

				srcpath=''
				for path in "./$srcname" "${SRCDEST:-.}/$srcname"; do
					if [ -f "$path" ]; then
						srcpath="$path"
						break
					fi
				done
				case "$srcpath" in
				'')
					:;;
				*)
					msg "Found generated source file: %s" "$srcname"
					local dest="${WORKDIR}/staging/other/${srcurl##"$mirror"}"
					mkdir -p -- "${dest%/*}"
					if cp "$srcpath" "$dest"; then
						msg2 "%s staged on [%s]" "$srcname" other
						staged=true
					else
						error "Can't put %s on [%s]" "$srcname" other
						return 1
					fi
					;;
				esac
				break
			esac
		done
	done

	if $staged ; then
		return 0
	else
		error "Nothing was staged"
		return 1
	fi
}

main "$@"
