#!/bin/ksh
#
# purge.sh - Remove unwanted files from the package
#
# Copyright (c) 2008-2016 Pacman Development Team <pacman-dev@archlinux.org>
# Copyright (c) 2013-2016 Luke Shumaker <lukeshu@sbcglobal.net>
#
# Modifications to support Hypertools:
# Copyright (C) 2023 Hyperbola Project
#
# License: GNU GPLv2+
#
# This file is part of LibreFetch
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case ${LIBMAKEPKG_TIDY_PURGE_SH} in
'') false;;
*) return;;
esac
LIBMAKEPKG_TIDY_PURGE_SH=1

case $(set | /bin/grep '^LIBRARY=') in
'') LIBRARY='/usr/share/makepkg';;
esac

. ${LIBRARY}'/util/message.sh'
. ${LIBRARY}'/util/option.sh'

packaging_options+=('purge')
tidy_remove+=('tidy_purge')

tidy_purge() {
    if check_option 'purge' 'y'; then
        case ${PURGE_TARGETS[*]} in
        '')
            :
            ;;
        *)
            msg2 $(gettext 'Purging unwanted files...')
            for pt in ${PURGE_TARGETS[@]}; do
                case $(file ${pt} | sed 's|.\+directory|directory|') in
                'directory')
                    case ${pt} in
                    *'/'*)
                        rm -rf ${pt}
                        ;;
                    *)
                        find '.' -type 'd' -name ${pt} -exec rm -rf -- '{}' +
                        ;;
                    esac
                    ;;
                *)
                    case ${pt} in
                    *'/'*)
                        rm -f ${pt}
                        ;;
                    *)
                        find '.' ! -type 'd' -name ${pt} -exec rm -f -- '{}' +
                        ;;
                    esac
                    ;;
                esac
            done
            unset pt
            ;;
        esac
    fi
}
