`librefetch` respects `SOURCE_DATE_EPOCH`[1] for reproducible builds,
and has had this behavior from the start (though the variable name
wasn't there from the start, that came later).  But how can this be!?
If you asked h01ger (the guy heading up reproducible builds in Debian
and Fedora), tar needs to support `--clamp-mtime` to do this, and that
flag wasn't in any tar implementation when librefetch came into
existence.  The flag not yet being in upstream GNU tar was for a long
time the biggest blocker to most of upstream Debian being
reproducible!

It's trivial to just adjust the input files first:

	find . -exec touch --no-dereference --date=DATE -- {} +`

Of course, `--clamp-mtime`/`--mtime` is now in upstream GNU tar.  So
librefetch supporting this is no longer impressive.  But, librefetch
still isn't using `--mtime`.  Why?  Well, because it uses libarchive
`bsdtar`, not GNU tar, and bsdtar still doesn't have the flag.

[1]: https://reproducible-builds.org/specs/source-date-epoch/
