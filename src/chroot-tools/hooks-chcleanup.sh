#!/bin/ksh

# Copyright (C) 2013 Luke Shumaker <lukeshu@sbcglobal.net>
#
# Modifications to support Hypertools:
# Copyright (C) 2023 Hyperbola Project
#
# License: GNU GPLv2+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -eu

hook_pre_build+=('clean_chroot')
clean_chroot() (
    set +x
    if ${INCHROOT}; then
        cd '/startdir'
        $(librelib 'chroot/chcleanup')
    else
        librechroot ${librechroot_flags[@]} 'clean-pkgs'
    fi
)
