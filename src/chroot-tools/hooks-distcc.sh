#!/bin/ksh

# Copyright (C) 2013 Luke Shumaker <lukeshu@sbcglobal.net>
#
# Modifications to support Hypertools:
# Copyright (C) 2022-2024 Hyperbola Project
#
# License: GNU GPLv2+
#
# This file is part of Parabola.
#
# Parabola is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Parabola is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Parabola. If not, see <http://www.gnu.org/licenses/>.

set -eu

hook_pre_build+=("distcc_start")
hook_post_build+=("distcc_stop")

_distcc_check() {
	local copydir=$1
	local home=$2

	local files=(
		"$copydir/bin/distcc-tool"
		"$copydir/run/distcc-tool.pid"
		"$home/.makepkg.conf"
		"$home/.ssh/config"
	)

	local file_err='false'
	for file in ${files[@]}; do
		if [ -f ${file} ]; then
			file_err='true'
			error 'Auto-generated file already exists, remove it: %s' ${file}
		fi
	done
	if ${file_err}; then
		exit 1
	fi
}

distcc_start() {
	local copydir=$1

	# Because /{,usr/}{,s}bin are all symlinked together for
	# fileystem 2013.05-2 and up (Parabola).
	if ${NONET} && [ -f ${copydir}'/bin/socat' ] && [ -f ${copydir}'/bin/distcc' ]; then
		local home
		if $INCHROOT; then
			home=$LIBREHOME
		else
			home="$copydir/build"
		fi

		_distcc_check

		local _distcc_tool="$(librelib chroot/distcc-tool)"
		install -m755 "$_distcc_tool"  "$copydir/bin/distcc-tool"

		mkdir -p "$home/.ssh"

		printf '%s\n' \
			'/bin/distcc-tool idaemon "$DISTCC_HOSTS" &' \
			'DISTCC_HOSTS="$(/bin/distcc-tool rewrite "$DISTCC_HOSTS")"' \
			> "$home/.makepkg.conf"

		printf '%s\n' \
			'Host *' \
			'    ProxyCommand /bin/distcc-tool client %h %p' \
			> "$home/.ssh/config"

		"$_distcc_tool" odaemon "$copydir" &
		echo $! > "$copydir/run/distcc-tool.pid"
	fi
}

distcc_stop() {
	local copydir=$1

	local home
	if ${INCHROOT}; then
		home=${LIBREHOME}
	else
		home=${copydir}'/build'
	fi

	if [ -f ${copydir}'/run/distcc-tool.pid' ]; then
		odaemon=$(< "$copydir/distcc-tool.pid")
		kill -- "$odaemon"

		rm -f -- \
			"$home/.makepkg.conf" \
			"$home/.ssh/config" \
			"$copydir/bin/distcc-tool" \
			"$copydir/run/distcc-tool.pid"
	fi
}
