#!/bin/ksh

# Copyright (C) 2013 Luke Shumaker <lukeshu@sbcglobal.net>
#
# Modifications to support Hypertools:
# Copyright (C) 2020-2024 Hyperbola Project
# Copyright (C) 2020 André Silva <emulatorman@hyperbola.info>
# Copyright (C) 2020-2024 Márcio Silva <coadde@hyperbola.info>
#
# License: GNU GPLv2+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -eu

DOAS_CONF='permit nopass keepenv setenv { HOME='${LIBREHOME}' } '${LIBREUSER}
IFS_BUFF=$IFS
rcode=0

hook_check_pkgbuild+=('check_pkgbuild_nonfree')
check_pkgbuild_nonfree()
{
    IFS=''
    if [ $(doas /bin/grep -qs ${DOAS_CONF} '/etc/doas.conf'; echo $?) != 0 ]
    then
        echo ${DOAS_CONF} >> ${copydir}'/etc/doas.conf'
    fi
    doas -u ${LIBREUSER} \
      ksh -c 'DOAS_USER='${DOAS_USER}' pkgbuild-check-nonfree -f' \
      || rcode=$?
    sed -i '\|'${DOAS_CONF}'|d' ${copydir}'/etc/doas.conf'
    IFS=${IFS_BUFF}
    pkgbuild-summarize-nonfree ${rcode}
}

#hook_check_pkgbuild+=('check_pkgbuild_namcap')
check_pkgbuild_namcap() {
    IFS=''
    if [ $(doas /bin/grep -qs ${DOAS_CONF} '/etc/doas.conf'; echo $?) != 0 ]
    then
        echo ${DOAS_CONF} >> ${copydir}'/etc/doas.conf'
    fi
    doas -u ${LIBREUSER} ksh -c 'DOAS_USER='$DOAS_USER" namcap 'PKGBUILD'"
    sed -i '\|'${DOAS_CONF}'|d' ${copydir}'/etc/doas.conf'
    IFS=${IFS_BUFF}
}
