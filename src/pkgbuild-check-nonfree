#!/bin/ksh

# pkgbuild-check-nonfree
#
# Copyright (C) 2011 Joseph Graham (Xylon) <joe@t67.eu>
# Copyright (C) 2010-2011 Joshua Ismael Haase Hernández (xihh) <hahj87@gmail.com>
# Copyright (C) 2010-2012 Nicolás Reynolds <fauno@parabola.nu>
# Copyright (C) 2012-2013 Luke Shumaker <lukeshu@sbcglobal.net>
#
# Modifications to support Hypertools:
# Copyright (C) 2022-2024 Hyperbola Project
#
# License: GNU GPLv3+
#
# This file is part of Parabola.
#
# Parabola is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parabola is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Parabola.  If not, see <http://www.gnu.org/licenses/>.

. "$(librelib messages)"
. "$(librelib conf)"

usage() {
	print "Usage: %s [OPTIONS] [PKGBUILD1 PKGBUILD2 ...]" "${0##*/}"
	print "Analyzes a PKGBUILD for freedom issues"
	echo
	prose 'If no PKGBUILD is specified, `./PKGBUILD` is implied.'
	echo
	print "Exit status (add them for combinations):"
	print "   0: Everything OK, no freedom issues"
	print "   1: Ran with error"
	print "Warning-level freedom issues:"
	print "   2: Uses unrecognized licenses, check them"
	print "   4: Uses GPL-incompatible licenses"
	print "Error-level freedom issues:"
	print "   8: Uses known unacceptable licenses"
	print "  16: Has nonfree dependencies"
	print "  32: Is a known nonfree package"
	echo
	print "Options:"
	flag '-f'            'Allow running as root user'
	echo
	flag '-q'            'Be quiet'
	flag '-v'            'Be verbose'
	echo
	flag '-h'            'Show this message'
}
# Make sure these match pkgbuild-summarize-nonfree
readonly _E_OK=0
readonly _E_ERROR=1
readonly _E_LIC_UNKNOWN=2
readonly _E_LIC_NOGPL=4
readonly _E_LIC_WRONG=8

main() {
	# Parse flags
	local asroot=false
	local v=1
	while getopts 'cfqvh' arg; do
		case "$arg" in
			f) asroot=true;;
			q) v=0;;
			v) v=2;;
			h) usage; return $_E_OK;;
			*) usage >&2; return $_E_ERROR;;
		esac
	done
	shift $((OPTIND - 1))
	if [ $# -lt 1 ]; then
		pkgbuilds=("`pwd`/PKGBUILD")
	else
		pkgbuilds=("$@")
	fi

	# Do a check to see if we are running as root
	if [ -w / ] && ! $asroot; then
		error "Run as normal user, or use the -f option to run as root."
		return 1
	fi

	# Adjust the verbosity
	if [ $v -eq 0 ]; then
		error() { :; }
		warning() { :; }
		plain() { :; }
		info() { :; }
	elif [ $v -eq 1 ]; then
		info() { :; }
	elif [ $v -eq 2 ]; then
		info() { plain "$@"; }
	fi

	# Do the work
	typeset -i ret=0
	local pkgbuild
	for pkgbuild in "${pkgbuilds[@]}"; do
		pkgbuild_check "$pkgbuild" || ret=$((ret | $?))
	done
	return $ret
}

# Helper functions #############################################################
# These should maybe be moved into lib/conf.sh

# Usage: var="$(pkgbuild_get_pkg_str ${pkgname} ${varname})"
# Gets a package-level string for a split-package
pkgbuild_get_pkg_str() {
	[ $# -eq 2 ] || panic 'malformed call to pkgbuild_get_pkg_str'
	local pkg=$1
	local var=$2

	local indirect=${!var}
	eval $(typeset -f package_$pkg | sed -rn "s/^\s*${var}(\+?=)/indirect\1/p")
	printf '%s' "${indirect}"
}
# Usage: eval $(pkgbuild_get_pkg_ary ${pkgname} ${varname} [$variable_name_to_set])
# Gets a package-level array for a split-package
pkgbuild_get_pkg_ary() {
	[ $# -eq 2 ] || [ $# -eq 3 ] || panic 'malformed call to pkgbuild_get_pkg_ary'
	local pkg=$1
	local var=$2
	local out=${3:-${var}}

	local ary=${var}'[@]'
	local indirect=(${!ary})
	eval $(typeset -f package_${pkg} | sed -rn 's|^\s*'${var}'(\+?=)|indirect\1|p')
	set | /bin/grep '^indirect=' | sed 's| indirect=| '${out}'=|'
}

# Checker functions ############################################################

# Usage: check_lic "${licence}"
# Check a license name to see if it is OK
check_lic() {
	[ $# -eq 1 ] || panic 'malformed call to check_license'
	local license=$1

	info 'Checking license: %s' "$license"

	if [ -e "/usr/share/licenses/common/$license" ]; then
		return $_E_OK
	else
		case "${license}" in
			BSD|BSD[0-9])
				warning "%s license is ambiguous, use 'Simplified-BSD', 'Modified-BSD', 'Clear-BSD', 'Original-BSD' or 'custom:BSD-License-Name' license." "$license"
				return $_E_LIC_WRONG;;
			MIT)
				warning "%s license is ambiguous, use 'Expat' or 'X11' license." "$license"
				return $_E_LIC_WRONG;;
			Original-BSD)
				warning "The 4-clause BSD license is free but has practical problems."
				return $_E_LIC_NOGPL;;
			custom:*)
				warning "'%s' license is not a common." "$license"
				return $_E_LIC_UNKNOWN;;
			*)
				warning "'%s' license is not a common, wrong typing or non-free." "$license"
				warning "If the %s license is not a common, please use 'custom:%s' license name." "$license"
				warning "Or if the %s license is a wrong typing, please use license name from '/usr/share/licenses/common/'." "$license"
				return $_E_LIC_WRONG;;
		esac
	fi
	panic 'code should never be reached'
}

# Usage: pkgbuild_ckec $pkgbuild
# Check whether a PKGBUILD has any issues (using the above)
pkgbuild_check() (
	[ $# -eq 1 ] || panic 'malformed call to pkgbuild_check'
	local pkgbuild=$1

	load_PKGBUILD "$pkgbuild"
	case $pkgname in
	'')
		return $_E_ERROR # not a PKGBUILD
		;;
	esac

	typeset -i ret=0 # the return status
	local dep lic # iterators for us in `for` loops
	local ck_deps ck_lics # lists of deps and licenses that have been checked

	if [ ${#pkgname[@]} -eq 1 ]; then
		msg2 'Inspecting package pkgname=%q (%s)' "$pkgname" "$(get_full_version)"
	else
		msg2 'Inspecting split package pkgbase=%q (%s)' "${pkgbase:-${pkgname[0]}}" "$(get_full_version)"
	fi

	# Check the licenses
	for lic in "${license[@]}"; do
		check_lic "$lic" || ret=$((ret | $?))
		ck_lics+=("$lic")
	done

	if [ ${#pkgname[@]} -eq 1 ]; then
		# Non-split package
		# Make sure a license is set
		if [ ${#ck_lics[@]} -eq 0 ]; then
			error "The license array is empty"
			ret=$((ret | _E_ERROR))
		fi
	else
		# Split package
		# Check the individual split packages
		local _pkgname _license _depends _optdepends
		for _pkgname in "${pkgname[@]}"; do
			msg2 'Inspecting split package pkgname=%q (%s)' "$_pkgname" "$(get_full_version "$_pkgname")"
			eval $(pkgbuild_get_pkg_ary "$_pkgname" license _license)
			eval $(pkgbuild_get_pkg_ary "$_pkgname" depends _depends)
			eval $(pkgbuild_get_pkg_ary "$_pkgname" optdepends _optdepends)

			# Check the licenses
			for lic in "${_license[@]}"; do
				if ! in_array "$lic" "${ck_lics[@]}"; then
					check_lic "$lic" || ret=$((ret | $?))
				fi
			done

			if [ ${#_license[@]} -eq 0 ]; then
				error "The license array is empty"
				ret=$((ret | _E_ERROR))
			fi
		done
	fi

	return $ret
)

main "$@"
