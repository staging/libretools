Special stuff about hacking ih the /src/lib directory:

 - Everything should be GPLv2 AND GPLv3 compatible.  No GPLv3 only.
 - Name a file `libre${NAME}` if it should be executable directly, or
   `${name}.sh` if it should only be available to be sourced.
 - When printing a message that is internal to /src/lib, and not part
   of the programm calling the library; prefix the print command with
   `_l`.  `_l()` is defined in `common.sh` (and `librelib`, since it
   cannot use any libraries itself).
 - When changing the message functions, be aware that some are
   duplicated in:
    * /src/chroot-tools/chcleanup
    * /src/chroot-tools/distcc-tool
    * /src/lib/librelib
   And that they probably need to be updated as well.
