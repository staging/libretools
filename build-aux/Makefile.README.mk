# This Makefile is a minimal stub that exists to allow the
# `at-modules` set of Make targets to print documentation for the
# present Autothing modules.
#
# This file is part of the documentation for Autothing.
#
# Copyright (C) 2017  Luke Shumaker
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.

dist.pkgname = autothing
dist.version = 1.0
gnuconf.pkgname = autothing

topoutdir ?= .
topsrcdir ?= .
include $(topsrcdir)/build-aux/Makefile.head.mk
include $(topsrcdir)/build-aux/Makefile.tail.mk
