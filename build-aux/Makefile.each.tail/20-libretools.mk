# Copyright (C) 2015-2017  Luke Shumaker
#
# Modifications to support Hypertools:
# Copyright (C) 2022 Hyperbola Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

ifneq ($(sort $(wildcard $(addprefix $(srcdir)/,$(libretools.src.devtools)))),$(sort $(addprefix $(srcdir)/,$(libretools.src.devtools))))
ifeq ($(wildcard $(devtoolsdir)/),)
$(error config.mk:devtoolsdir points to a non-existant directory: $(devtoolsdir))
endif
endif

# Connect with `files` module ########################################
files.src.gen += $(libretools.src.devtools)
files.out.all += $(filter-out $(files.src),$(libretools.out)) \
                 $(if $(strip $(libretools.pots)),everything.pot)
files.sys.all += $(addprefix $(bindir)/,$(libretools.out.bins)) \
                 $(addprefix $(pkgconfdir)/,$(libretools.out.confs)) \
                 $(addprefix $(pkglibexecdir)/,$(libretools.out.libexecs) $(libretools.out.libs)) \
                 $(addprefix $(pkgdocdir)/,$(libretools.out.docs)) \
                 $(addprefix $(mandir)/man1/,$(filter %.1,$(libretools.out.mans))) \
                 $(addprefix $(mandir)/man2/,$(filter %.2,$(libretools.out.mans))) \
                 $(addprefix $(mandir)/man3/,$(filter %.3,$(libretools.out.mans))) \
                 $(addprefix $(mandir)/man4/,$(filter %.4,$(libretools.out.mans))) \
                 $(addprefix $(mandir)/man5/,$(filter %.5,$(libretools.out.mans))) \
                 $(addprefix $(mandir)/man6/,$(filter %.6,$(libretools.out.mans))) \
                 $(addprefix $(mandir)/man7/,$(filter %.7,$(libretools.out.mans))) \
                 $(addprefix $(mandir)/man8/,$(filter %.8,$(libretools.out.mans)))
files.out.int += *.pot *.ugly *.rej *.orig

# Generate rules #####################################################
$(srcdir)/%.in: $(devtoolsdir)/%.in
	cp -T '$<' '$@'
$(srcdir)/%.in: $(devtoolsdir)/lib/%
	cp -T '$<' '$@'

# Build rules ########################################################
_is_executable = $(filter $(libretools.out.bins) $(libretools.out.libexecs),$(@F))

$(outdir)/%: $(srcdir)/%.in
	@echo 'EDIT < $< > $@'; $(libretools.edit) < '$<' | install -T -m$(if $(_is_executable),755,644) /dev/stdin '$@'
$(outdir)/%: $(srcdir)/%.ronn
	ronn --roff $(RONNFLAGS) < '$<' > '$@'
$(outdir)/%.html: $(srcdir)/%.ronn
	ronn --html $(RONNFLAGS) < '$<' > '$@'
$(outdir)/%.pot: $(outdir)/% $(topsrcdir)/src/lib/librexgettext
	$(topsrcdir)/src/lib/librexgettext $(LIBREXGETTEXT_FLAGS) '$<' > '$@'
$(outdir)/%.pot: $(srcdir)/% $(topsrcdir)/src/lib/librexgettext
	$(topsrcdir)/src/lib/librexgettext $(LIBREXGETTEXT_FLAGS) '$<' > '$@'
$(outdir)/everything.pot: $(addprefix $(outdir)/,$(addsuffix .pot,$(libretools.pots)))
	cat $^ | $(libretools.pofmt) > '$@'

# Install rules ######################################################
$(DESTDIR)$(pkgconfdir)/%   : $(outdir)/%  ; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(pkgdocdir)/%    : $(outdir)/%  ; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man1/%.1: $(outdir)/%.1; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man2/%.2: $(outdir)/%.2; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man3/%.3: $(outdir)/%.3; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man4/%.4: $(outdir)/%.4; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man5/%.5: $(outdir)/%.5; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man6/%.6: $(outdir)/%.6; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man7/%.7: $(outdir)/%.7; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(mandir)/man8/%.8: $(outdir)/%.8; install -T -Dm644 '$<' '$@'
$(DESTDIR)$(pkglibexecdir)/%: $(outdir)/%  ; $(MKDIR_P) '$(@D)' && cp -T '$<' '$@'
$(DESTDIR)$(bindir)/%       : $(outdir)/%  ; install -T -Dm755 '$<' '$@'
# Repeat the last two rules again with explicit targets
ifneq ($(filter $(notdir $(outdir)),$(libretools.out)),)
$(DESTDIR)$(pkglibexecdir)/$(notdir $(outdir)): $(DESTDIR)$(pkglibexecdir)/%: $(outdir)/%  ; $(MKDIR_P) '$(@D)' && cp -T '$<' '$@'
$(DESTDIR)$(bindir)/$(notdir        $(outdir)): $(DESTDIR)$(bindir)/%       : $(outdir)/%  ; install -T -Dm755 '$<' '$@'
endif
