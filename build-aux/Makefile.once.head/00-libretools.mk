# Copyright (C) 2015, 2017  Luke Shumaker
#
# Modifications to support Hypertools:
# Copyright (C) 2022-2024 Hyperbola Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

mod.libretools.description = (libretools) build rules
define mod.libretools.doc
# Inputs:
#   - Install paths (Directory variables): 
#     - `pkgconfdir    = $(sysconfdir)/libretools.d`
#     - `pkgdocdir     = $(docdir)/libretools`
#     - `pkglibexecdir = $(libexecdir)/libretools`
#   - Outputs (Directory variables): 
#     - `libretools.out.mans`
#     - `libretools.out.bins`
#     - `libretools.out.libexecs`
#     - `libretools.out.libs`
#     - `libretools.out.docs`
#     - `libretools.out.confs`
#     - `libretools.out`
#   - Misc (Directory variables):
#     - `libretools.src.devtools`
#     - `libretools.pots`
# Outputs:
#   - Autothing module configuration (Global variables):
#     - `gitfiles.file`
#     - `var`
#   - Shell commands (Global variables)
#     - `libretools.edit`
#     - `libretools.pofmt`
#   - Source information
#     - Directory variable : `libretools.src`
#     - Directory variable : `libretools.src.exec`
#     - Directory variable : `libretools.src.conf`
#     - Directory variable : `libretools.src.ronn`
#     - Directory variable : `libretools.src.md`
#     - Directory variable : `libretools.src.sh`
#     - File               : `$(topsrcdir)/.srcversion-libretools.mk`
#     - Global variable    : `LIBRETOOLS_VERSION`
#     - Global variable    : `LIBRETOOLS_COMMIT`
#     - File               : `$(topsrcdir)/.srcversion-devtools.mk`
#     - Global variable    : `DEVTOOLS_VERSION`
#     - Global variable    : `DEVTOOLS_COMMIT`
#   - Primary outputs:
#     - Directory variable : `files.src.gen`
#     - Directory variable : `files.out.all`
#     - Directory variable : `files.sys.all`
#     - Directory variable : `files.out.int`
#     - Target             : `$(srcdir)/%.in: $(devtoolsdir)/%.in`
#     - Target             : `$(outdir)/%: $(srcdir)/%.in`
#     - Target             : `$(outdir)/%     : $(srcdir)/%.ronn`
#     - Target             : `$(outdir)/%.html: $(srcdir)/%.ronn`
#     - Target             : `$(outdir)/%.pot: $(outdir)/%`
#     - Target             : `$(outdir)/%.pot: $(srcdir)/%`
#     - Target             : `$(outdir)/everything.pot`
#     - Target             : `$(outdir)/%.ugly: $(srcdir)/%.in $(srcdir)/%.patch`
#     - Target             : `$(outdir)/%: $(outdir)/%.ugly
#     - Target             : `$(DESTDIR)...:`
# Misc:
#   - Variable: `LIBREXGETTEXT_FLAGS`
endef
mod.quote.doc := $(value mod.quote.doc)

# Configure how Make works ###########################################
.SECONDARY:
.DELETE_ON_ERROR:

# Configure how Autothing modules work ###############################
gitfiles.file = .srcfiles.mk

# Boilerplate ########################################################
.PHONY: FORCE

# Commands ###########################################################

# Usage: <INPUT $(libretools.edit) >OUTPUT
# Expand m4_include macros to use librelib
# This is used when using sources grabbed from devtools
# Magic for doing @variable@ replacement in files
libretools.edit = sed \
	-e 's|^\#!\s*/bin/bash|\#!/bin/ksh|' \
	-e 's|m4_include(lib/\(.*\))|. "$$(librelib \1)"|' \
	$(foreach v,$(patsubst $(var.)%,%,$(filter $(var.)%,$^)), -e 's|@$(v)@|$($(v))|g' )

# Auto-detect ########################################################

# It's easy to think of these as "each" variables, but because they
# will be evaluated on demand, only files.src.src needs to be "each".
libretools.src.git  = $(files.src.src)
libretools.src.exec = $(patsubst $(srcdir)/%,%,$(shell find $(addprefix $(srcdir)/,$(libretools.src.git)) -executable 2>/dev/null))
libretools.src.conf = $(filter %.conf,$(libretools.src.git))
libretools.src.sh   = $(filter %.sh  ,$(libretools.src.git))
libretools.src.ronn = $(filter %.ronn,$(libretools.src.git))
libretools.src.md   = $(filter-out HACKING.md,$(filter %.ronn %.md,$(libretools.src.git)))

LIBREXGETTEXT_FLAGS ?=

# Git Version ########################################################

-include $(topsrcdir)/.srcversion-libretools.mk
-include $(topsrcdir)/.srcversion-devtools.mk

ifneq ($(wildcard $(topsrcdir)/.git),)
$(topsrcdir)/.srcversion-libretools.mk: FORCE
	@{ \
		echo LIBRETOOLS_VERSION = $(patsubst v%,%,$(shell cd $(topsrcdir) && git describe --tags)); \
		echo LIBRETOOLS_COMMIT = $(shell cd $(topsrcdir) && git rev-parse HEAD); \
	:; } | $(WRITE_IFCHANGED) $@
endif

ifneq ($(wildcard $(devtoolsdir)/.git),)
$(topsrcdir)/.srcversion-devtools.mk: FORCE
	@{ \
		echo DEVTOOLS_VERSION = $(patsubst libretools-%,%,$(shell cd $(devtoolsdir) && git describe --tags)); \
		echo DEVTOOLS_COMMIT = $(shell cd $(devtoolsdir) && git rev-parse HEAD); \
	:; } | $(WRITE_IFCHANGED) $@
endif
