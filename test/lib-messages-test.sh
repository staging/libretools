#!/usr/bin/env roundup

describe libremessages
. ./test-common.sh

it_can_be_included_twice() (
	. libremessages
	. libremessages
)

it_can_be_included_with_set_euE() (
	set -eu
	. libremessages
)

it_works_with_no_color_and_set_euE() {
	(
		unset TERM
		set -eu
		. libremessages
		msg Foo
	) >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

it_can_be_called_without_including() {
	libremessages msg Foo >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

it_fails_with_msg_and_no_args() {
	ret=0
	libremessages msg || ret=$?
	[ $ret -ne 0 ]
}

it_allows_subheadings_to_flag() {
	# Note that old versions of `flag` panicked if given an odd
	# number of headings, so give an odd number here.
	libremessages flag \
		      -a adesc \
		      -b bdesc \
		      Head1: \
		      -c cdesc > $tmpdir/out
	printf '%s\n' \
	       '  -a            adesc' \
	       '  -b            bdesc' \
	       ' Head1:' \
	       '  -c            cdesc' > $tmpdir/exp
	diff -u $tmpdir/exp $tmpdir/out
}
