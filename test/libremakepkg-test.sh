#!/usr/bin/env roundup

describe libremakepkg
. ./test-common.sh

rcode=0

setup_chrootdir

it_builds_a_trivial_package() {
	require network doas || return 0
	cp libremakepkg.d/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas libremakepkg -l "$roundup_test_name"

	[ -f $(echo libretools-hello-1.0-1-any.pkg.tar.?z) ]
}

it_enables_networking_during_prepare() {
	require network doas || return 0
	cp libremakepkg.d/PKGBUILD-netprepare "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas libremakepkg -l "$roundup_test_name"
	[ -f $(echo libretools-netprepare-1.0-1-any.pkg.tar.?z) ]
}

it_disables_networking_during_build() {
	require network doas || return 0
	cp libremakepkg.d/PKGBUILD-netbuild "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	not testdoas libremakepkg -l "$roundup_test_name"
	not [ -f $(echo libretools-netbuild-1.0-1-any.pkg.tar.?z) ]
	testdoas libremakepkg -l "$roundup_test_name" -N
	[ -f $(echo libretools-netbuild-1.0-1-any.pkg.tar.?z) ]
}

it_disables_networking_during_package() {
	require network doas || return 0
	cp libremakepkg.d/PKGBUILD-netpackage "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	not testdoas libremakepkg -l "$roundup_test_name"
	not [ -f $(echo libretools-netpackage-1.0-1-any.pkg.tar.?z) ]
	testdoas libremakepkg -l "$roundup_test_name" -N
	[ -f $(echo libretools-netpackage-1.0-1-any.pkg.tar.?z) ]
}

it_cleans_the_chroot_before_building() {
	require network doas || return 0
	# 1. First, we build testpkg1
	# 2. Then, we build testpkg2, which depends on testpkg1
	#    Therefore, testpkg1 will be installed after testpkg2 is built, we
	#    check for that.
	# 3. Then, we build hello, which depends on neither, so testpkg1 should
	#    be removed.

	# Also, do funny things with the output of libremakepkg to get a helpful
	# fail case.

	mkdir -p "$tmpdir"/{1,2,3}
	cp libremakepkg.d/PKGBUILD-testpkg1 "$tmpdir/1/PKGBUILD"
	cp libremakepkg.d/PKGBUILD-testpkg2 "$tmpdir/2/PKGBUILD"
	cp libremakepkg.d/PKGBUILD-hello    "$tmpdir/3/PKGBUILD"

	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty

	cd "$tmpdir/1"
	testdoas libremakepkg -l "$roundup_test_name" &> "$tmpdir/out" || { r=$?; tail "$tmpdir/out"|cat -v; return $r; }

	cd "$tmpdir/2"
	testdoas libremakepkg -l "$roundup_test_name" &> "$tmpdir/out" || { r=$?; tail "$tmpdir/out"|cat -v; return $r; }
	testdoas librechroot -l "$roundup_test_name" run libretools-testpkg1 'first time, pass'

	# This next line is actually a separate test, but it fits in well with this test, and chroot tests are slow..
	# it_doesnt_cache_local_packages() {
	not testdoas librechroot -l "$roundup_test_name" run test -e /var/cache/pacman/pkg/libretools-testpkg1-1.0-1-any.pkg.tar.?z

	cd "$tmpdir/3"
	testdoas libremakepkg -l "$roundup_test_name" &> "$tmpdir/out" || { r=$?; tail "$tmpdir/out"|cat -v; return $r; }
	not testdoas librechroot -l "$roundup_test_name" run libretools-testpkg1 'second time, fail'
}

it_handles_PKGDEST_not_existing() {
	require network doas || return 0
	cp libremakepkg.d/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas env PKGDEST="$tmpdir/dest/pkgdest" libremakepkg -l "$roundup_test_name"

	[ -f $(echo dest/pkgdest/libretools-hello-1.0-1-any.pkg.tar.?z) ]
}

it_displays_help_as_normal_user() {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C libremakepkg -h >$tmpdir/stdout 2>$tmpdir/stderr

	case $(sed 1q $tmpdir/stdout) in
	Usage:*) true;;
	*) false;;
	esac
	empty $tmpdir/stderr
}

it_otherwise_fails_as_normal_user() {
	# this gives a chance of passing
	cp libremakepkg.d/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	libremakepkg >$tmpdir/stdout 2>$tmpdir/stderr || rcode=$?

	[ ${rcode} -ne 0 ]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

it_fails_if_a_hook_fails() {
	require network doas || return 0
	cp libremakepkg.d/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	trap 'sed -i s/-bogus// "$XDG_CONFIG_HOME"/libretools/libretools.conf' RETURN

	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas libremakepkg -l "$roundup_test_name" >$tmpdir/stdout 2>$tmpdir/stderr || rcode=$?

	[ ${rcode} -ne 0 ]
	tail -n1 $tmpdir/stderr | grep -qF '==> ERROR: Failure(s) in check_pkgbuild: check_pkgbuild_nonfree'
}
