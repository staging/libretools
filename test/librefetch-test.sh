#!/usr/bin/env roundup

describe librefetch
. ./test-common.sh

KEYSERVER=hkp://pool.sks-keyservers.net
GPG="gpg --quiet --batch --no-tty --no-permission-warning --keyserver ${KEYSERVER}"
SRCBALL='testpkg-1.0.tar.gz'

common_before() {
	mkdir -p "$XDG_CONFIG_HOME"/{pacman,libretools} "$tmpdir/srcdest"

	cat <<EOF > "$XDG_CONFIG_HOME/pacman/makepkg.conf"
DLAGENTS=('ftp::/usr/bin/curl -fC - --ftp-pasv --retry 3 --retry-delay 3 -o %o %u'
          'http::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -o %o %u'
          'https::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -o %o %u'
          'rsync::/usr/bin/rsync --no-motd -z %u %o'
          'scp::/usr/bin/scp -C %u %o')
BUILDDIR=""
SRCDEST=$tmpdir/srcdest
. ${_librelib_conf_sh_pkgconfdir}/librefetch-makepkg.conf
EOF
	sed -i 's,/usr/bin/librefetch,$(which librefetch),' \
	    "${_librelib_conf_sh_pkgconfdir}/librefetch-makepkg.conf"

	export MAKEPKG_CONF="$XDG_CONFIG_HOME/pacman/makepkg.conf"

	printf '%s\n' \
		'MIRRORS=("phony://example.com/dir/")' \
		'DOWNLOADER=/usr/bin/false' \
		> "$XDG_CONFIG_HOME/libretools/librefetch.conf"

	printf '%s\n' \
	       'Key-Type: RSA' \
	       'Key-Length: 1024' \
	       'Key-Usage: sign' \
	       'Name-Real: Temporary LibreTools testsuite key' \
	       'Name-Email: libretools-test@localhost' \
	       'Expire-Date: 0' \
	       '%no-protection' \
	       '%commit' \
	    | $GPG --gen-key 2>/dev/null
}

it_displays_help() {
	LC_ALL=C librefetch -h >$tmpdir/stdout 2>$tmpdir/stderr

	case $(sed 1q $tmpdir/stdout) in
	Usage:*) true;;
	*) false;;
	esac
	empty $tmpdir/stderr
}

# This test used to be called "it_cleans_src_libre_first", but let's
# be honest: it checks pretty much everything related to normal
# operation.
it_runs() {
	cp 'librefetch.d/'* ${tmpdir}
	cd ${tmpdir}

	# Create garbage, to verifiy that it cleans src-libre first
	mkdir -p 'src-libre/foo'
	touch 'src-libre/foo/file'

	# Run librefetch
	makepkg -g

	# Verify that no temporary files were left around
	not test -e 'librefetch.'*

	# Verify:
	# - The SRCBALL was created...
	# - ... and is in the correct directory
	# - The SRCBALL does not contain the garbage created earlier
	# - The files in the SRCBALL are in the correct order (if the
	#   order isn't ensured, then this would only sometimes fail,
	#   unfortunately).
	bsdtar 'tf' ${tmpdir}'/srcdest/'${SRCBALL} > 'list-pkg.txt'
	diff -u 'list.txt' 'list-pkg.txt'
	# Verify that the signature was created and matches
	gpg --quiet \
	    --verify ${tmpdir}'/srcdest/'${SRCBALL}'.sig' \
	    ${tmpdir}'/srcdest/'${SRCBALL} 2> '/dev/null'
}

it_recurses() {
	cp 'librefetch.d/'* ${tmpdir}
	cd ${tmpdir}
	mv 'PKGBUILD{-recurse' 'PKGBUILD'

	makepkg -g
	bsdtar 'tf' ${tmpdir}'/srcdest/'${SRCBALL} > 'list-pkg.txt'
	diff -u 'list.txt' 'list-pkg.txt'
	gpg --quiet \
	    --verify ${tmpdir}'/srcdest/'${SRCBALL}'.sig' \
	    ${tmpdir}'/srcdest/'${SRCBALL} 2> '/dev/null'
}
