#!/usr/bin/env roundup

describe is_built
. ./test-common.sh

it_displays_help() {
	LC_ALL=C is_built -h >$tmpdir/stdout 2>$tmpdir/stderr

	case $(sed 1q $tmpdir/stdout) in
	Usage:*) true;;
	*) false;;
	esac
	empty $tmpdir/stderr
}

it_fails_with_0_args() {
	is_built >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -gt 1 ]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

it_succeeds_with_1_arg() {
	is_built sh >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	empty $tmpdir/stderr
}

it_returns_1_for_non_existent_package() {
	is_built phony-ne-package 100 >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -eq 1 ]
	empty $tmpdir/stdout
	empty $tmpdir/stderr
}

it_returns_1_for_future_packages() {
	# If emacs ever goes rapid release, we might need to change this :P
	is_built emacs 100 >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -eq 1 ]
	empty $tmpdir/stdout
	empty $tmpdir/stderr
}

it_returns_0_for_past_packages() {
	# If emacs ever goes rapid release, we might need to change this :P
	is_built emacs 1 >$tmpdir/stdout 2>$tmpdir/stderr

	empty $tmpdir/stdout
	empty $tmpdir/stderr
}
