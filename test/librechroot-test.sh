#!/usr/bin/env roundup

describe librechroot
. ./test-common.sh

setup_chrootdir

it_creates_repo_for_new_chroots() {
	require network doas || return 0
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas librechroot -l "$roundup_test_name" run test -r /repo/repo.db
}

it_cleans_the_local_repo_correctly() {
	require network doas || return 0
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas librechroot -l "$roundup_test_name" make
	testdoas librechroot -l "$roundup_test_name" clean-repo
	testdoas librechroot -l "$roundup_test_name" run test -r /repo/repo.db
	# TODO: inspect /repo/* more
}

it_respects_exit_status_if_out_isnt_a_tty() (
	require network doas || return 0
	set -o pipefail
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	r=0
	{ testdoas librechroot -l "$roundup_test_name" run ksh -c 'exit 3' | cat; } || r=$?

	[ $r -eq 3 ]
)

it_creates_ca_certificates() {
	require network doas || return 0
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas librechroot -l "$roundup_test_name" run test -r /etc/ssl/certs/ca-certificates.crt
}

it_disables_networking_when_requested() {
	require network doas || return 0
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty

	    testdoas librechroot -l "$roundup_test_name"    run curl https://repo.hyperbola.info:50011/ >/dev/null
	not testdoas librechroot -l "$roundup_test_name" -N run curl https://repo.hyperbola.info:50011/ >/dev/null
}

it_handles_CHROOTEXTRAPKG_correctly() {
	requuire network doas || return 0
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty

	not testdoas librechroot -l "$roundup_test_name" run lsof
	echo "CHROOTEXTRAPKG=(lsof)" >> "$XDG_CONFIG_HOME"/libretools/chroot.conf
	testdoas librechroot -l "$roundup_test_name" install-name lsof
	testdoas librechroot -l "$roundup_test_name" clean-pkgs
	testdoas librechroot -l "$roundup_test_name" run lsof
	echo "CHROOTEXTRAPKG=()" >> "$XDG_CONFIG_HOME"/libretools/chroot.conf
	testdoas librechroot -l "$roundup_test_name" clean-pkgs
	not testdoas librechroot -l "$roundup_test_name" run lsof
}

it_displays_help_as_normal_user() {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C librechroot help >$tmpdir/stdout 2>$tmpdir/stderr

	case $(sed 1q $tmpdir/stdout) in
	Usage:*) true;;
	*) false;;
	esac
	empty $tmpdir/stderr
}

it_otherwise_fails_as_normal_user() {
	librechroot -l "$roundup_test_name" run true >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -ne 0 ]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

it_displays_help_and_fails_with_0_args() {
	LC_ALL=C librechroot -l "$roundup_test_name" >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -ne 0 ]
	empty $tmpdir/stdout
	case $(sed -n 2q $tmpdir/stderr) in
	Usage:*) true;;
	*) false;;
	esac
}

# requires doas so we know it's not failing because it needs root
it_fails_for_unknown_commands() {
	require doas || return 0
	testdoas librechroot phony >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -ne 0 ]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

# requires doas so we know it's not failing because it needs root
it_fails_for_unknown_flags() {
	require doas || return 0
	testdoas librechroot -q >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -ne 0 ]
	empty $tmpdir/stdout
	not empty $tmpdir/stderr
}

it_fails_when_syncing_a_copy_with_itself() {
	require doas || return 0
	testdoas timeout 5 librechroot -l root sync  || stat=$?
	case $stat in
		0|124|137) # success|timeout+TERM|timeout+KILL
			false;;
		*)
			true;;
	esac
}

it_deletes_copies() {
	require network doas || return 0
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas librechroot -l "$roundup_test_name" make
	test -d "$chrootdir/default/$roundup_test_name"
	testdoas librechroot -l "$roundup_test_name" delete
	not test -e "$chrootdir/default/$roundup_test_name"
}

it_deletes_subvolumes_recursively() {
	require network doas btrfs || return 0
	libremessages msg 'Creating a chroot, may take a few minutes' &>/dev/tty
	testdoas librechroot -l "$roundup_test_name" make
	testdoas librechroot -l "$roundup_test_name" run pacman -S --noconfirm btrfs-progs
	test -d "$chrootdir/default/$roundup_test_name"
	not test -e "$chrootdir/default/$roundup_test_name/var/subvolume"
	testdoas librechroot -l "$roundup_test_name" run btrfs subvolume create /var/subvolume
	test -d "$chrootdir/default/$roundup_test_name/var/subvolume"
	testdoas librechroot -l "$roundup_test_name" delete
	not test -e "$chrootdir/default/$roundup_test_name"
}
