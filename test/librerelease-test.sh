#!/usr/bin/env roundup

describe librestage
. ./test-common.sh

common_before() {
	mkdir -p $XDG_CONFIG_HOME/libretools
	{
		echo "WORKDIR='$tmpdir/workdir'"
		echo 'REPODEST=repo@repo:/srv/http/repo/staging-$LIBREUSER'
	} >$XDG_CONFIG_HOME/libretools/libretools.conf
	{
		echo 'PKGEXT=.pkg.tar.gz'
		echo "PKGDEST='$tmpdir/workdir/pkgdest'"
		echo "GPGKEY=YOURKEY"
	} > $HOME/.makepkg.conf
	mkdir -p "$tmpdir/workdir/pkgdest"
}

it_displays_usage_text() {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C librerelease -h >"$tmpdir/stdout" 2>"$tmpdir/stderr"

	case $(sed 1q $tmpdir/stdout) in
	Usage:*) true;;
	*) false;;
	esac
	empty "$tmpdir/stderr"
}

it_lists_all_files() {
	WORKDIR="$tmpdir/workdir"
	mkdir -p "$WORKDIR/staging/repo1" "$WORKDIR/staging/repo2/sub"
	touch \
		"$WORKDIR/staging/repo1/file1" \
		"$WORKDIR/staging/repo1/file2" \
		"$WORKDIR/staging/repo2/file with spaces" \
		"$WORKDIR/staging/repo2/sub/subfolder"
	unset WORKDIR
	LC_ALL=C librerelease -l &>"$tmpdir/list"

	cat > "$tmpdir/list-correct" <<EOF
  -> repo1
     file1
     file2
  -> repo2
     file with spaces
     sub/subfolder
EOF

	diff "$tmpdir/list-correct" "$tmpdir/list"
}
