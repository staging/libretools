#!/hint/ksh

case $HOME in
"$(eval echo ~$USER)")
	libremessages error "\$HOME is the default for %s; use testenv: %s" "$USER" "$HOME"
	exit 1
	;;
esac

DOAS_EXEC='doas'
missing=

common_before() {
	:
}

common_after() {
	:
}

before() {
	unset PKGDEST SRCDEST SRCPKGDEST LOGDEST
	unset BUILDDIR
	unset PKGEXT SRCEXT
	unset GPGKEY PACKAGER
	killall gpg-agent &>/dev/null || true

	tmpdir="$(mktemp -d --tmpdir "test-${roundup_desc//\//-}.${roundup_test_name}.XXXXXXXXXXXX")"
	chmod 755 "$tmpdir"

	stat=0

	common_before
}

after() {
	common_after
	killall gpg-agent &>/dev/null || true
	if [ -f "$tmpdir/.used-doas" ]; then
		doas rm -rf -- "$tmpdir" "$XDG_CONFIG_HOME" "$XDG_CACHE_HOME"
	else
		rm -rf -- "$tmpdir" "$XDG_CONFIG_HOME" "$XDG_CACHE_HOME"
	fi
}

setup_chrootdir() {
	case "$chrootdir" in
	'')
		export chrootdir="$(mktemp -d --tmpdir "test-chrootdir.XXXXXXXXXXXX")"
		trap "$(printf '_cleanup_chrootdir %q' "$chrootdir")" EXIT
		;;
	esac
	common_before() {
		mkdir -p "$XDG_CONFIG_HOME"/libretools

		echo "CHROOTDIR='${chrootdir}'" > "$XDG_CONFIG_HOME"/libretools/chroot.conf
		echo "CHROOT='default'" >> "$XDG_CONFIG_HOME"/libretools/chroot.conf
		echo "CHROOTEXTRAPKG=()" >> "$XDG_CONFIG_HOME"/libretools/chroot.conf
	}
}

_cleanup_chrootdir() (
	case ${DOAS} in
	'') DOAS_EXEC='';;
	*) DOAS_EXEC='doas';;
	esac

	if [ -d $1 ]; then
		case $(stat -f -c '%T' $1) in
		'btrfs')
			${DOAS_EXEC} find $1 -depth -inum 256 \
			    -exec btrfs 'subvolume' 'delete' '{}' \; \
			    &> '/dev/null'
			;;
		esac
	fi
	[ -e $1 ] && ${DOAS_EXEC} rm -rf -- $1
)

require() (
	set +x

	if libremessages 'in_array' 'network' $@; then
		case ${NETWORK} in
		'') missing+='networking ';;
		esac
	fi

	if libremessages 'in_array' 'doas' $@; then
		case ${DOAS} in
		'') missing+='doas ';;
		esac
	fi

	if libremessages 'in_array' 'btrfs' $@; then
		case $(stat -f -c '%T' ${chrootdir} 2> '/dev/null' || true) in
		'btrfs') :;;
		*) missing+='btrfs ';;
		esac
	fi

	if [ ${#missing} -ne 0 ]; then
		libremessages 'warning' \
		    'Next test requires %s; Skipping (passing)...' \
		    $(echo ${missing} | sed 's| |, |g') &> '/dev/tty'
		return 1
	fi

	if libremessages 'in_array' 'doas' $@; then
		touch ${tmpdir}'/.used-doas'
	fi

	return 0
)

empty() (
	set +x
	[ $(stat -c %s "$1") -eq 0 ]
)

# Just using '!' doesn't trip `set -e`
not() (
	set +x
	! eval "$@"
)
