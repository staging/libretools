#!/usr/bin/env roundup

describe lib/conf.sh
. ./test-common.sh

it_sets_makepkg_vars_in_custom_file() {
	unset PKGDEST
	touch "$tmpdir/makepkg.conf"
	. $(librelib conf.sh)
	MAKEPKG_CONF="$tmpdir/makepkg.conf" set_var makepkg PKGDEST /pkgdest
	.  "$tmpdir/makepkg.conf"
	case $PKGDEST in
	/pkgdest) true;;
	*) false;;
	esac
}

it_figures_out_HOME_when_root() {
	require doas || return 0
	# This one is tricky, because it does the job too well, it will find
	# the actual HOME, instead of the test environment HOME.  Therefore, we
	# will just check that case $HOME in /root) false;; esac
	cd "$tmpdir"
	echo '. $(librelib conf.sh); echo "$LIBREHOME"' > test.sh
	LIBREHOME=$(testdoas ksh ./test.sh)
	case $LIBREHOME in
	/root) false;;
	*) true;;
	esac
}

it_respects_custom_HOME() {
	cd "$tmpdir"
	echo '. $(librelib conf.sh); echo "$LIBREHOME"' > test.sh

	export HOME=/foo
	LIBREHOME=$(ksh ./test.sh)

	case $LIBREHOME in
	/foo) true;;
	*) false;;
	esac
}
