#!/usr/bin/env roundup

describe librelib
. ./test-common.sh

it_displays_help_and_fails_with_0_args() {
	ret=0
	librelib >$tmpdir/stdout 2>$tmpdir/stderr || ret=$?

	empty $tmpdir/stdout
	case $(sed 1q $tmpdir/stderr) in
	Usage:*) true;;
	*) false;;
	esac
	[ $ret -ne 0 ]
}

it_fails_with_2_args() {
	ret=0
	librelib a b >$tmpdir/stdout 2>$tmpdir/stderr || ret=$?

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	[ $ret -ne 0 ]
}

it_displays_usage_text() {
	librelib -h >$tmpdir/stdout 2>$tmpdir/stderr

	case $(sed 1q $tmpdir/stdout) in
	Usage:*) true;;
	*) false;;
	esac
	empty $tmpdir/stderr
}

# Nothing in $(libdir) should be executable anymore (except that
# $(libexecdir)=$(libdir), and executable things go in there. But I
# digress, libremessages should not be executable anymore).
it_finds() {
	v1=$(librelib $1)
	v2=$(librelib $2)
	v3=$(librelib $3)
	v4=$(librelib $4)

	[ -r "$v1" ] && [ ! -x "$v1" ]
	case "$v1" in
	"$v2") true;;
	"$v3") true;;
	"$v4") true;;
	*) false;;
	esac
}

it_finds_messages() {
	it_finds messages libremessages messages.sh libremessages.sh
}

# conf.sh is non-executable
it_finds_conf() {
	it_finds conf libreconf conf.sh libreconf.sh
}

it_fails_to_find_phony() {
	ret=0
	librelib phony >$tmpdir/stdout 2>$tmpdir/stderr || ret=$?

	empty $tmpdir/stdout
	not empty $tmpdir/stderr
	[ $ret -ne 0 ]
}
