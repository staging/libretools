#!/usr/bin/env roundup

describe librestage
. ./test-common.sh

common_before() {
	mkdir -p $XDG_CONFIG_HOME/libretools
	echo "WORKDIR='$tmpdir/workdir'" > $XDG_CONFIG_HOME/libretools/libretools.conf
	echo "ARCHES=('x86_64' 'i686' 'misp64el')" >> $XDG_CONFIG_HOME/libretools/libretools.conf

	echo 'PKGEXT=.pkg.tar.gz' > $HOME/.makepkg.conf
	echo "PKGDEST='$tmpdir/workdir/pkgdest'" >> $HOME/.makepkg.conf
	echo "PACKAGER='Test Suite <test@localhost>'" >> $HOME/.makepkg.conf
	mkdir -p "$tmpdir/workdir/pkgdest"
}

it_displays_usage_text() {
	rm -rf "$XDG_CONFIG_HOME"
	LC_ALL=C librestage -h >$tmpdir/stdout 2>$tmpdir/stderr

	case $(sed 1q $tmpdir/stdout) in
	Usage:*) true;;
	*) false;;
	esac
	empty "$tmpdir/stderr"
}

it_fails_with_0_args() {
	librestage >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -ne 0 ]
	empty "$tmpdir/stdout"
	not empty "$tmpdir/stderr"
}

it_fails_with_invalid_args() {
	librestage -q >$tmpdir/stdout 2>$tmpdir/stderr || stat=$?

	[ $stat -ne 0 ]
	empty "$tmpdir/stdout"
	not empty "$tmpdir/stderr"
}

it_guesses_the_repo() {
	mkdir -p -- "$tmpdir/reponame/libretools-hello"
	cp librestage.d/PKGBUILD-hello "$tmpdir/reponame/libretools-hello/PKGBUILD"
	cd "$tmpdir/reponame/libretools-hello"

	makepkg
	librestage

	[ -f $tmpdir/workdir/staging/reponame/libretools-hello-1.0-1-any.pkg.tar.gz ]
}

it_stages_packages_without_PKGDEST() {
	echo "PKGDEST=''" >> $HOME/.makepkg.conf

	cp librestage.d/PKGBUILD-hello "$tmpdir/PKGBUILD"
	cd "$tmpdir"

	makepkg
	librestage repo1

	[ -f $tmpdir/workdir/staging/repo1/libretools-hello-1.0-1-any.pkg.tar.gz ]
}
